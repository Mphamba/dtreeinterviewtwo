package com.smphamba.dtreeinterviewtwo.ui

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.DarkMode
import androidx.compose.material.icons.filled.LightMode
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.smphamba.dtreeinterviewtwo.Config
import com.smphamba.dtreeinterviewtwo.R
import com.smphamba.dtreeinterviewtwo.data.model.Data
import com.smphamba.dtreeinterviewtwo.ui.viewmodel.MainUiViewModel
import com.smphamba.dtreeinterviewtwo.util.Status

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomePage() {
    Scaffold(modifier = Modifier.fillMaxSize(), topBar = {
        SmallTopAppBar(title = { Text(text = "People") }, actions = {
            IconButton(onClick = { Config.isDarkTheme.value = !Config.isDarkTheme.value }) {
                val icon =
                    if (Config.isDarkTheme.value) Icons.Default.DarkMode else Icons.Default.LightMode
                Icon(imageVector = icon, contentDescription = null)
            }
        })
    }) { innerPadding ->

        val context = LocalContext.current
        val mainUiViewModel: MainUiViewModel = hiltViewModel()

        val uiState = mainUiViewModel.uiState


        val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = true)
        val loadingState = mainUiViewModel.loadingData.collectAsState(initial = null).value

        // Initiate api call to load data once the view is created
        LaunchedEffect(key1 = true) {
            mainUiViewModel.event(MainUiEvents.LoadData)
        }

        Column(modifier = Modifier.padding(innerPadding)) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedTextField(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 8.dp),
                    label = { Text(text = "Filter by city name") },
                    value = uiState.cityNameFilter,
                    onValueChange = {
                        mainUiViewModel.event(MainUiEvents.FilterData(it))
                    })

                Button(onClick = { mainUiViewModel.event(MainUiEvents.SubmitFilter) }) {
                    Text(text = "Filter")
                }
            }

            Spacer(modifier = Modifier.height(8.dp))
            SwipeRefresh(
                state = swipeRefreshState,
                onRefresh = { mainUiViewModel.event(MainUiEvents.LoadData) }) {

                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(uiState.dataList) { data ->
                        DataItemUi(data)
                    }
                }


            }
        }

        when (loadingState?.status) {
            Status.SUCCESS -> {
                swipeRefreshState.isRefreshing = false
            }

            Status.ERROR -> {
                swipeRefreshState.isRefreshing = false
                Toast.makeText(context, loadingState.message, Toast.LENGTH_LONG).show()
            }

            Status.LOADING -> {
                swipeRefreshState.isRefreshing = true
            }

            else -> Unit
        }
    }
}

@Composable
fun DataItemUi(data: Data) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 8.dp, bottom = 8.dp, end = 8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 4.dp, top = 4.dp, bottom = 4.dp, end = 8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = R.drawable.avartar),
                contentDescription = null,
                modifier = Modifier
                    .clip(shape = CircleShape)
                    .size(50.dp)
            )

            Column(
                Modifier
                    .weight(1f)
                    .padding(start = 8.dp)
            ) {
                Row(modifier = Modifier.fillMaxWidth()) {
                    Text(
                        text = "${data.name} ${data.surname}",
                        style = MaterialTheme.typography.titleMedium,
                        modifier = Modifier.weight(1f)
                    )
                    Text(
                        text = "${data.age} yrs",
                        style = MaterialTheme.typography.bodyMedium,
                        color = MaterialTheme.colorScheme.primary
                    )


                }
                Text(text = data.city, style = MaterialTheme.typography.bodyMedium)
            }
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewUi() {
    HomePage()
}