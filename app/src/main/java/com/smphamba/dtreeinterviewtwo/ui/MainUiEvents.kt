package com.smphamba.dtreeinterviewtwo.ui

sealed class MainUiEvents {
    object LoadData: MainUiEvents()
    class FilterData(val cityName: String): MainUiEvents()
    object SubmitFilter: MainUiEvents()
}
