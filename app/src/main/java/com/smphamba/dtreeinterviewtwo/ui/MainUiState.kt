package com.smphamba.dtreeinterviewtwo.ui

import com.smphamba.dtreeinterviewtwo.data.model.Data

data class MainUiState(
    val dataList: List<Data> = emptyList(),
    val cityNameFilter: String = ""
)
