package com.smphamba.dtreeinterviewtwo.ui.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.smphamba.dtreeinterviewtwo.data.model.Data
import com.smphamba.dtreeinterviewtwo.data.repo.DataRepo
import com.smphamba.dtreeinterviewtwo.ui.MainUiEvents
import com.smphamba.dtreeinterviewtwo.ui.MainUiState
import com.smphamba.dtreeinterviewtwo.util.Resource
import com.smphamba.dtreeinterviewtwo.util.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainUiViewModel @Inject constructor(
    private val dataRepo: DataRepo
) : ViewModel() {

    var uiState by mutableStateOf(MainUiState())

    private val loadingDataEventChannel = Channel<Resource<List<Data>>>()
    val loadingData = loadingDataEventChannel.consumeAsFlow()

    private fun getData(){
        viewModelScope.launch(Dispatchers.IO) {
            dataRepo.getData().collectLatest {
                if (it.status == Status.SUCCESS) {
                    uiState = uiState.copy(dataList = it.data ?: emptyList())
                }
                loadingDataEventChannel.send(it)
            }
        }
    }

    private fun getDataByCity(){
        if (uiState.cityNameFilter.isEmpty()){
            getData()
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            dataRepo.getDataByCityName(uiState.cityNameFilter).collectLatest {
                if (it.status == Status.SUCCESS) {
                    uiState = uiState.copy(dataList = it.data ?: emptyList())
                }
                loadingDataEventChannel.send(it)
            }
        }
    }

    fun event(event: MainUiEvents) {
        when(event) {
            is MainUiEvents.LoadData ->  getData()
            is MainUiEvents.FilterData -> {
//                getDataByCity()
            }
            is MainUiEvents.SubmitFilter -> {
                getDataByCity()
            }
        }
    }

}