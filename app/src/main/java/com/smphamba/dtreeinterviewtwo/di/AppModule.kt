package com.smphamba.dtreeinterviewtwo.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.smphamba.dtreeinterviewtwo.Config
import com.smphamba.dtreeinterviewtwo.data.repo.DataRepo
import com.smphamba.dtreeinterviewtwo.data.retrofit.ApiInterceptor
import com.smphamba.dtreeinterviewtwo.data.retrofit.ApiServiceInterface
import com.smphamba.dtreeinterviewtwo.util.NetworkUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class AppModule {
    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    fun provideRetrofit(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .client(NetworkUtils.httpClient.build())
            .baseUrl(Config.API_ADDRESS)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun provideRetrofitApiService(retrofit: Retrofit.Builder): ApiServiceInterface {
        return retrofit.build()
            .create(ApiServiceInterface::class.java)
    }

    @Singleton
    @Provides
    fun provideDataRepo(apiServiceInterface: ApiServiceInterface) = DataRepo(apiServiceInterface)
}