package com.smphamba.dtreeinterviewtwo

import androidx.compose.runtime.mutableStateOf

object Config {
    const val API_ADDRESS = "https://exercise-646d.restdb.io/rest/"
    val isDarkTheme = mutableStateOf(false)
}