package com.smphamba.dtreeinterviewtwo.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParseException
import com.smphamba.dtreeinterviewtwo.data.retrofit.ApiInterceptor
import okhttp3.*
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.internal.http2.ConnectionShutdownException
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object NetworkUtils {

    const val OKHTTP_CUSTOM_ERROR_CODE = 501
    const val DEFAULT_ERROR_MESSAGE = "Error processing your request, Please try again."
    const val API_KEY = "globaj5s_callmob"

    val nullOnEmptyConverterFactory = object : Converter.Factory() {
        fun converterFactory() = this
        override fun responseBodyConverter(
            type: Type,
            annotations: Array<out Annotation>,
            retrofit: Retrofit
        ) = object :
            Converter<ResponseBody, Any?> {
            val nextResponseBodyConverter =
                retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)

            override fun convert(value: ResponseBody) = try {
                if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("FATAL_ERROR", e.message.toString())
                null
            }
        }
    }

    private fun isConnected(context: Context): Boolean {
        var result = false
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm?.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    result = when {
                        hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                }
            }
        } else {
            cm?.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = true
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = true
                    }
                }
            }
        }
        return result
    }

    private val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    val httpClient =
        OkHttpClient.Builder().addInterceptor(Interceptor.invoke { chain ->
            val request = chain.request()
            try {
                val response = chain.proceed(request)
                val bodyString = response.body!!.string()
                return@invoke response.newBuilder()
                    .body(bodyString.toResponseBody(response.body?.contentType()))
                    .build()
            } catch (e: Exception) {
                e.printStackTrace()
                var msg = ""
                when (e) {
                    is SocketTimeoutException -> {
                        msg = "Timeout - Please check your internet connection"
                    }
                    is UnknownHostException -> {
                        msg = "Unable to make a connection. Please check your internet"
                    }
                    is ConnectionShutdownException -> {
                        msg = "Connection shutdown. Please check your internet"
                    }
                    is JsonParseException -> {
                        msg = "Unrecognised Server, please confirm domain and try again."
                    }
                    is IOException -> {
                        msg = "Server is unreachable, please confirm domain and try again."
                    }
                    is IllegalStateException -> {
                        msg = "${e.message}"
                    }
                    else -> {
                        msg = "${e.message}"
                    }
                }
                return@invoke Response.Builder()
                    .request(request)
                    .protocol(Protocol.HTTP_1_1)
                    .code(OKHTTP_CUSTOM_ERROR_CODE)
                    .message(msg)
                    .body("{${e}}".toResponseBody(null)).build()
            }
        }).addInterceptor(loggingInterceptor)
            .addInterceptor(ApiInterceptor())

    val gson: Gson = GsonBuilder().setLenient().create()

}