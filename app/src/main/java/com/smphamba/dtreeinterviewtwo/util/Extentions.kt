package com.smphamba.dtreeinterviewtwo.util

import com.smphamba.dtreeinterviewtwo.data.model.Data
import com.smphamba.dtreeinterviewtwo.data.retrofit.dto.DataDto

fun DataDto.toData(): Data {
    return Data(
        id = _id ?: "",
        personId = id ?: "",
        name = name ?: "",
        surname = surname ?: "",
        age = age ?: 0,
        city = city ?: "",
        parentId = parentid ?: ""
    )
}