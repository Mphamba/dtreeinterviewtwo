package com.smphamba.dtreeinterviewtwo.data.retrofit

import com.google.gson.JsonObject
import com.smphamba.dtreeinterviewtwo.data.retrofit.dto.DataDto
import retrofit2.http.*

interface ApiServiceInterface {
    @GET("group-1")
    suspend fun getData(): List<DataDto>

    @GET("group-1")
    suspend fun getDataByCity(@Query("q") q : JsonObject): List<DataDto>
}


