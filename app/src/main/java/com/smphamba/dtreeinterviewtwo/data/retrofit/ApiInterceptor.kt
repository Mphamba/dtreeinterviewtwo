package com.smphamba.dtreeinterviewtwo.data.retrofit

import okhttp3.Interceptor

/**
 *  an API gateway server built for accepting API requests from the client
 *  applications and routing them to the appropriate backend services
 */
class ApiInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder()
            .header("Content-Type", "application/json")
            .header("x-apikey", "5c5c7076f210985199db5488")
            .header("cache-control", "no-cache")
            .build()
        return chain.proceed(request)
    }
}
