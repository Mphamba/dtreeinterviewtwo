package com.smphamba.dtreeinterviewtwo.data.model

data class Data(
    val id: String,
    val personId: String,
    val name: String,
    val surname: String,
    val age: Int,
    val city: String,
    val parentId: String
)