package com.smphamba.dtreeinterviewtwo.data.retrofit.dto

import com.google.gson.annotations.SerializedName

// Data Transfer object, modeling data from api
data class DataDto (
    val _id: String?,

    @SerializedName("ID")
    val id: String?,

    @SerializedName("NAME")
    val name: String?,

    @SerializedName("SURNAME")
    val surname: String?,

    @SerializedName("AGE")
    val age: Int?,

    @SerializedName("CITY")
    val city: String?,

    @SerializedName("PARENTID")
    val parentid: String?
)



//{"_id":"5c5c6e6ec83a924e00028dd4","NAME":"grace","SURNAME":"Devji","AGE":8,"CITY":"Dar Es Salaam","ID":10239,"PARENTID":48203},