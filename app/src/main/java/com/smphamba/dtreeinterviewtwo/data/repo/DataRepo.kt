package com.smphamba.dtreeinterviewtwo.data.repo

import com.google.gson.JsonObject
import com.smphamba.dtreeinterviewtwo.data.model.Data
import com.smphamba.dtreeinterviewtwo.data.retrofit.ApiServiceInterface
import com.smphamba.dtreeinterviewtwo.util.Resource
import com.smphamba.dtreeinterviewtwo.util.toData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DataRepo(private val apiServiceInterface: ApiServiceInterface) {
    fun getData(): Flow<Resource<List<Data>>> = flow {
        emit(Resource.loading(null))
        try {
            val response = apiServiceInterface.getData()
            emit(Resource.success(response.map { it.toData() }))
        } catch (e: Exception) {
            emit(Resource.error(e.message, null))
        }
    }

    fun getDataByCityName(cityName: String) = flow {
        emit(Resource.loading(null))
        try {
            val arg = JsonObject()
            arg.addProperty("CITY", cityName)
            val response = apiServiceInterface.getDataByCity(arg)
            emit(Resource.success(response.map { it.toData() }))
        } catch (e: Exception) {
            emit(Resource.error(e.message, null))
        }
    }
}